package ccondom

import (
	"bytes"
	"crypto/sha256"
	"encoding/base32"
	"encoding/base64"
	"errors"
	"net"
	"time"
	"sync"
)

var (
	i2pB64enc *base64.Encoding = base64.NewEncoding("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-~")
	i2pB32enc *base32.Encoding = base32.NewEncoding("abcdefghijklmnopqrstuvwxyz234567")
)





// I2P address (b64-encoded destination).
type I2PAddr string

// Creates a new I2P address from a base64-encoded string.
func NewI2PAddrFromString(addr string) (I2PAddr, error) {
	// very basic check
	if len(addr) > 4096 || len(addr) < 516 {
		return I2PAddr(""), errors.New("Not an I2P address")
	}
	buf := make([]byte, 4096)
	if _, err := i2pB64enc.Decode(buf, []byte(addr)); err != nil {
		return I2PAddr(""), errors.New("Address is not base64-encoded")
	}
	return I2PAddr(addr), nil
}

// Creates a new I2P address from a byte array (pure binary, as you would have 
// if you have *not* base64-encoded it.)
func NewI2PAddrFromBytes(addr []byte) (I2PAddr, error) {
	if len(addr) > 4096 || len(addr) < 384 {
		return I2PAddr(""), errors.New("Not an I2P address")
	}
	buf := make([]byte, i2pB64enc.EncodedLen(len(addr)))
	i2pB64enc.Encode(buf, addr)
	return I2PAddr(string(buf)), nil
}

// Shortcut to keys.Addr(). Will return the wrong result, if the I2P address has a certificate. Use the much slower BOBController.GetI2PAddr() for best compability.
func NewI2PAddrFromKeys(keys I2PKeys) I2PAddr {
	return keys.Addr()
}

// Returns "I2P"
func (I2PAddr) Network() string {
	return "I2P"
}

// Returns the base64 encoded I2P address
func (a I2PAddr) String() string {
	return string(a)
}

// Returns the *.b32.i2p address of the I2P address.
func (addr I2PAddr) Base32() string {
	hash := sha256.New()
	hash.Write([]byte(string(addr)))
	digest := hash.Sum(nil)
	b32addr := make([]byte, 56)
	i2pB32enc.Encode(b32addr, digest)
	return string(b32addr[:52]) + ".b32.i2p"
}

// I2P keys. Consists of public and private keys, and maybe a certificate. The public key is the I2PAddr.
type I2PKeys string

// Creates new I2PKeys from a string, if that string looks like a pair of I2P keys
// encoded in I2Ps base64-format (alphabet [A-Z][a-z][0-9]-~).
// This function does not check if your keys are actually real keys, so if they pass
// the basic sanity check, and they are bad, there will be errors in the future 
// (probably when trying to Listen() at them, or similiar.)
func I2PKeysFromString(keys string) (I2PKeys, error) {
	// very basic check
	if len(keys) > 4096 || len(keys) < 800 {
		return I2PKeys(""), errors.New("Not I2P keys")
	}
	buf := make([]byte, 4096)
	if _, err := i2pB64enc.Decode(buf, []byte(keys)); err != nil {
		return I2PKeys(""), errors.New("Keys are not base64-encoded")
	}
	return I2PKeys(keys), nil
}

// Creates new I2PKeys from a []byte. Performs basic sanity check. Does not check
// if your keys actually are real I2P keys.
func I2PKeysFromBytes(keys []byte) (I2PKeys, error) {
	if len(keys) > 4096 || len(keys) < 600 {
		return I2PKeys(""), errors.New("Not I2P keys")
	}
	buf := make([]byte, i2pB64enc.EncodedLen(len(keys)))
	i2pB64enc.Encode(buf, keys)
	return I2PKeys(string(buf)), nil
}

// Returns the I2P address (the public keys), from the I2PKeys. Will return the wrong result, if the I2P address has a certificate. Use the much slower BOBController.GetI2PAddr() for best compability.
func (k I2PKeys) Addr() I2PAddr {
	buf := make([]byte, i2pB64enc.EncodedLen(384))
	i2pB64enc.Encode(buf, k.Bytes()[:384])
	return I2PAddr(string(buf) + "AAAA")
}

// Returns the keys encoded in I2Ps base64-alphabet. (516 runes or longer.)
func (k I2PKeys) String() string {
	return string(k)
}

// Returns the bytes of the keys (384+ bytes)
func (k I2PKeys) Bytes() []byte {
	keys := make([]byte, i2pB64enc.DecodedLen(len(k)))
	_, err := i2pB64enc.Decode(keys, []byte(k))
	if err != nil {
		panic("Failed to base64-decode I2PKeys: I2PKeys.Bytes() failed.")
	}
	return keys
}





// A connection to something in the I2P cipherspace.
type I2PConn struct {
	remote           I2PAddr
	local            I2PAddr
	conn             net.Conn           // the normal conn that connects to the I2P tunnel
}

// Implements net.Conn
func (c *I2PConn) Read(b []byte) (int, error) {
	n,err := c.conn.Read(b)
	return n,err
}

// Implements net.Conn
func (c *I2PConn) Write(b []byte) (int, error) {
	n,err := c.conn.Write(b)
	return n,err
}

// Implements net.Conn
func (c *I2PConn) Close() error {
	return c.conn.Close()
}

// Implements net.Conn
func (c *I2PConn) LocalAddr() net.Addr {
	return c.local
}

// Implements net.Conn
func (c *I2PConn) RemoteAddr() net.Addr {
	return c.remote
}

// Implements net.Conn
func (c *I2PConn) SetDeadline(t time.Time) error {
	return c.SetDeadline(t)
}

// Implements net.Conn
func (c *I2PConn) SetReadDeadline(t time.Time) error {
	return c.SetReadDeadline(t)
}

// Implements net.Conn
func (c *I2PConn) SetWriteDeadline(t time.Time) error {
	return c.SetWriteDeadline(t)
}





// Defines an ingoing I2P tunnel.
type I2PListener struct {
	TunnelName       string
	BobController    *BOBController
	laddr            I2PAddr
	proxyListener    net.Listener       // the BOBController uses this to accept tunnels from BOB

	err              error
	l                sync.Mutex
}

const defaultListenReadLen = 516

// Accept waits for and returns the next connection to the listener.
func (l *I2PListener) Accept() (net.Conn, error) {
	l.l.Lock()
	if l.err != nil {
		l.l.Unlock()
		return nil, l.err
	}
	l.l.Unlock()
	
	conn, err := l.proxyListener.Accept()
	if err != nil {
		l.Close()
		return nil, errors.New("I2P tunnel failure.")
	}
	// translate l.conn into an I2PConn
	buf := make([]byte, defaultListenReadLen)
	n, err := conn.Read(buf)
	if n < 6 {
		return nil, errors.New("Failed to establish connection. Unknown reply.")
	}
	if bytes.HasPrefix(buf[:n], []byte("ERROR ")) {
		return nil, errors.New(string(buf[:n]))
	}
	if n < defaultListenReadLen {
		return nil, errors.New("Got unknown message: " + string(buf[:n]))
	}
	
	// I2P inserts the I2P address ("destination") of the connecting peer into the datastream, followed by
	// a \n. Since the length of a destination may vary, this reads until a newline is found. At the time
	// of writing, the length is never less then, and almost always equals 516 bytes, which is why 
	// defaultListenReadLen is 516.
	if rune(buf[defaultListenReadLen - 1]) != '\n' {
		abuf := make([]byte, 1)
		for {
			n, err := conn.Read(abuf)
			if n != 1 || err != nil {
				return nil, errors.New("Failed to decode connecting peers I2P destination.")
			}
			buf = append(buf, abuf[0])
			if rune(abuf[0]) == '\n' { break }
		}
	}
	remoteAddr, err := NewI2PAddrFromString(string(buf[:len(buf)-1])) // the address minus the trailing newline
	if err != nil {
		l.Close()
		return nil, errors.New("Could not determine connecting tunnels address.")
	}
	return &I2PConn{remoteAddr, l.laddr, conn}, nil
}

// Close closes the listener.
// Any blocked Accept operations will be unblocked and return errors.
func (l *I2PListener) Close() error {
	l.l.Lock()
	if l.err != nil {
		l.l.Unlock()
		return l.err
	}
	l.err = errors.New("Listener is closed.")
	l.l.Unlock()
	
	err, _ := l.BobController.closeTunnel(l.TunnelName, 15)
	if err != nil {
		l.l.Lock()
		l.err = err
		l.l.Unlock()
		return err
	}
	err = l.proxyListener.Close()
	if err != nil {
		l.l.Lock()
		l.err = err
		l.l.Unlock()
		return errors.New("Could not close local port used for the BOB proxy. The I2P tunnel was closed.")
	}
	return nil
}

// Addr returns the listener's I2P destination
func (l *I2PListener) Addr() I2PAddr {
	return l.laddr
}



