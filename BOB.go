// Library for I2Ps BOB bridge (https://geti2p.com)
package ccondom

import (
	"errors"
	"net"
	"strconv"
	"math/rand"
	"strings"
	"time"
)


// I2P outgoing tunnel.
type I2POutgoingTunnel struct {
	Name             string              // The name of the I2PTunnel (will appear in the I2P console)
	Port             int                 // The port it is using (remote, at the address of the BOB proxy!)
	BOBController    *BOBController      // BOBController that this tunnel belongs to
	keys             I2PKeys             // I2P keys for this tunnel
}




// Creates a new I2P tunnel for connecting to services in the I2P network, with
// length plus or minus lengthVariance hops in the network. If you give the name
// of "" it will create a new unique name for itself. The name *probably* needs
// to be something sane, like ASCII alphanumerics and dashes. The tunnel will 
// show up under its given name in I2Ps web console.
func (b *BOBController) NewOutgoingTunnel(tunnelName string, length, lengthVariance int) (*I2POutgoingTunnel, error) {
	if b.err != nil {
		return nil, b.err
	}
	if length < 0 {
		panic("NewOutgoingTunnel with length < 0")
	}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	rc := make(chan bobComReply)
	tname := tunnelName
	if tname == "" {
		tname = b.newTunnelName()
	}
	
	tkeys, err := b.NewKeys()
	if err != nil {
		return nil, errors.New("Could not generate new keys for the tunnel.")
	}
	
	// BOB appears to have no means to open a random port by itself for the
	// tunnel, but instead relies on the client to select one that is not 
	// already used. So we select a random number from 10000-65335 and if there
	// is a failure, we try again, for at most "tries" times.
	tries := 5
	var rport int
	for ; tries != 0; tries-- {
		rport = 10000 + r.Intn(65336-10000)
		comms := []string{"setnick " + tname,
			"setkeys " + tkeys.String(), 
			"option inbound.length=" + strconv.Itoa(length),
			"option outbound.length=" + strconv.Itoa(length),
			"option inbound.lengthVariance=" + strconv.Itoa(lengthVariance),
			"option outbound.lengthVariance=" + strconv.Itoa(lengthVariance),
			"inhost " + b.getRemoteAddr(), 
			"inport " + strconv.Itoa(rport), 
			"quiet true",
			"start"}
		b.request <- bobCommands{comms, rc, true}
		reply := <-rc
		if reply.err != nil && !strings.HasPrefix(reply.message, "inport") {
			return nil, errors.New("Unable to create I2P tunnel: " + reply.err.Error())
		}
		if reply.err == nil {
			break
		}
	}
	if tries == 0 {
		return nil, errors.New("Could not tell BOB to select a port at random.")
	}
	b.l.Lock()
	b.tunnels[tname]=0
	b.l.Unlock()
	return &I2POutgoingTunnel{Name: tname, Port: rport, BOBController: b, keys: tkeys}, nil
}



// Connects to *something* through I2P. address may be an *.b32.i2p address, a 
// normal .i2p address (like "ugha.i2p"), or a normal vanilla internet address 
// (like "duckduckgo.com:80"). I2P addresses do not have port numbers. Use 
// DialI2PAddr() to dial directly to an I2P destination.
// WARNING: If there is a connection error, I2P write "ERROR " + error message to
// this connection, which could be misinterpreted as real data being transmitted!
func (t *I2POutgoingTunnel) DialI2P(address string) (net.Conn, error) {
	t.BOBController.l.RLock()
	if t.BOBController.err != nil {
		t.BOBController.l.RUnlock()
		return nil, t.BOBController.err
	}
	t.BOBController.l.RUnlock()
	
	// This makes sure that the I2PConn has a valid remoteAddress
	ri2pAddr, err := NewI2PAddrFromString(address)
	if err != nil {
		ri2pAddr, err = t.BOBController.Lookup(address)
		if err != nil {
			return nil, errors.New("Could not determine remote I2P address: " + err.Error())
		}
	}
	
	tunnelAddress := t.BOBController.getRemoteAddr() + ":" + strconv.Itoa(t.Port)
	conn, err := net.Dial(t.BOBController.Network, tunnelAddress)
	if err != nil {
		return nil, errors.New("Could not connect to I2P Tunnel: " + err.Error())
	}
	_, err = conn.Write([]byte(ri2pAddr.String() + "\n"))
	if err != nil {
		return nil, errors.New("Could not connect through I2P tunnel: " + err.Error())
	}
	laddr := t.keys.Addr()
	return &I2PConn{ri2pAddr, laddr, conn}, nil
}

// Dials to an I2P destination.
// WARNING: If there is a connection error, I2P write "ERROR " + error message to
// this connection, which could be misinterpreted as real data being transmitted!
func (t *I2POutgoingTunnel) DialI2PAddr(dest I2PAddr) (net.Conn, error) {
	conn, err := t.DialI2P(dest.String())
	return conn, err
}

// Closes the tunnel.
func (t I2POutgoingTunnel) Close() error {
	err,_ := t.BOBController.closeTunnel(t.Name, 15)
	return err
}

// Performs a lookup of a name. Returns the I2P destination. Using the I2P 
// destination directly instead of the short variants is quicker, as no 
// additional lookup in the DHT is required.
func (b *BOBController) Lookup(name string) (I2PAddr, error) {
	b.l.RLock()
	if b.err != nil {
		b.l.RUnlock()
		return "", b.err
	}
	b.l.RUnlock()
	
	comms := []string{"lookup " + name}
	rc := make(chan bobComReply)
	b.request <- bobCommands{comms, rc, true}
	reply := <-rc
	if reply.err != nil {
		return "", errors.New("Lookup failed: " + reply.err.Error())
	}
	if strings.HasPrefix(reply.message, "OK") {
		dest := strings.TrimSpace(reply.message[2:])
		return I2PAddr(dest), nil
	} else {
		return "", errors.New("You need to update ccondom, BOB changed protocol replies.")
	}
}

// Creates a new keys for I2P. The public key is your I2P destination, which is
// an address in the I2P cipherspace, similiar to a TOR hidden service address
// but much longer. Notice that this is BOTH the public key and the private key.
// Do not share it with untrusted peers!!! See also GetDestination().
func (b *BOBController) NewKeys() (I2PKeys, error) {
	b.l.RLock()
	if b.err != nil {
		b.l.RUnlock()
		return "", b.err
	}
	b.l.RUnlock()
	
	var destination string
	tempName := b.newTunnelName()

	// this does not actually create a new tunnel, just a nickname.
	comms := []string{"setnick " + tempName, "newkeys", "getkeys"}
	rc := make(chan bobComReply)
	b.request <- bobCommands{comms, rc, true}
	reply := <-rc
	if reply.err != nil {
		return "", errors.New("Failed to generate new keys: " + reply.err.Error())
	}
	if strings.HasPrefix(reply.message, "OK") {
		destination = strings.TrimSpace(reply.message[2:])
	} else {
		return "", errors.New("You need to update ccondom, BOB changed protocol replies.")
	}

	// make sure the nickname is cleared from BOBs memory.
	comms = []string{"setnick " + tempName, "clear"}
	b.request <- bobCommands{comms, rc, false}
	reply = <-rc
	if reply.err != nil {
		return "", errors.New("Failed to clear nickname from BOB.")
	}
	return I2PKeys(destination), nil	
}

// Returns the address part of a pair of I2P keys, that is, the public key.
// Others can use it to connect to you. This function asks BOB to extract the
// public keys from your I2PKeys. It is *much* quicker to do the same thing using
// I2PKeys.Addr().
func (b *BOBController) GetI2PAddr(keys I2PKeys) (I2PAddr, error) {
	b.l.RLock()
	if b.err != nil {
		b.l.RUnlock()
		return "", b.err
	}
	b.l.RUnlock()
	
	tempName := b.newTunnelName()
	comms := []string{"setnick " + tempName, "setkeys " + string(keys), "getdest"}
	rc := make(chan bobComReply)
	b.request <- bobCommands{comms, rc, true}
	reply := <-rc
	if reply.err != nil {
		return "", errors.New("Failure: " + reply.err.Error())
	}
	var dest I2PAddr
	if strings.HasPrefix(reply.message, "OK") {
		dest = I2PAddr(strings.TrimSpace(reply.message[2:]))
	} else {
		return "", errors.New("You need to update ccondom, BOB changed protocol replies.")
	}

	// make sure the nickname is cleared from BOBs memory.
	comms = []string{"setnick " + tempName, "clear"}
	b.request <- bobCommands{comms, rc, false}
	reply = <-rc
	if reply.err != nil {
		return "", errors.New("Failed to clear nickname from BOB.")
	}
	return dest, nil
}



// Creates a new ingoing I2P tunnel that listens at using the keys you specified.
func (b *BOBController) Listen(tunnelName string, keys I2PKeys, length, lengthVariance int) (*I2PListener, error) {
	b.l.RLock()
	if b.err != nil {
		b.l.RUnlock()
		return nil, b.err
	}
	b.l.RUnlock()

	if tunnelName == "" {
		tunnelName = b.newTunnelName()
	}

	myAddr := &net.TCPAddr{IP: net.ParseIP(b.getLocalAddr()), Port: 0, Zone: ""}
	listener, err := net.ListenTCP("tcp", myAddr)
	if err != nil {
		return nil, err
	}
	_,lport,err := net.SplitHostPort(listener.Addr().String())
	if err != nil {
		return nil, err
	}
	comms := []string{"setnick " + tunnelName, 
		"setkeys " + string(keys), 
		"option inbound.length=" + strconv.Itoa(length),
		"option outbound.length=" + strconv.Itoa(length),
		"option inbound.lengthVariance=" + strconv.Itoa(lengthVariance),
		"option outbound.lengthVariance=" + strconv.Itoa(lengthVariance),
		"outhost " + b.getLocalAddr(),
		"outport " + lport,
		"start"}
	rc := make(chan bobComReply)
	b.request <- bobCommands{comms, rc, true}
	reply := <-rc
	if reply.err != nil {
		return nil, errors.New("Failure: " + reply.err.Error())
	}
	b.tunnels[tunnelName]=0
	i2pListener := &I2PListener{ TunnelName: tunnelName,
		BobController: b,
		laddr: keys.Addr(),
		proxyListener: listener,
		err: nil }
	return i2pListener, nil
}



