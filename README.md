# README #

Go library for making connections through I2P. It uses the BOB API/controller.

This library is really ugly looking on the inside.

Since it is using BOB for controlling I2P tunnels, if your program crash or if you fail to close the tunnels when your program exits, they will linger on until you either manually close the tunnels or you restart your I2P router. This is a feature-bug of BOB, and a reason not to use this library. A better alternative is probably to use a library for the SAMv3 API instead, such as: https://bitbucket.org/kallevedin/sam3

## Documentation ##

* Download ccondom
* Type `godoc -http=:8081` into your terminal and hit enter.
* goto http://localhost:8081 and click on packages, then navigate to ccondom.

## Example ##

```go
package main

import (
	"fmt"
	"bitbucket.org/kallevedin/ccondom"
)

var (
	YourBOBController   string = "127.0.0.1:2827"
	addr                string = "i2p-projekt.i2p"
)

main() {
	bob, _ := ccondom.NewBOBController("tcp4", YourBOBController)
	tunnel, _ := bob.NewOutgoingTunnel("MyTunnelName", 2, 1)
	conn, _ := tunnel.DialI2P(addr)

	raddr := conn.RemoteAddr()

	fmt.Println("Network: " + raddr.Network())
	fmt.Println("long destination: " + raddr.String()[:50] + "(...etc)")
	
	i2pAddr, _ := ccondom.NewI2PAddrFromString(raddr.String())
	fmt.Println("b32 destination: " + i2pAddr.Base32())
	
	buf := make([]byte, 4096)
	n, _ := conn.Read(buf)
	fmt.Println("Read: " + string(buf[:n]))
}
```

The above will print out

```text
Network: I2P
long destination: 8ZAW~KzGFMUEj0pdchy6GQOOZbuzbqpWtiApEj8LHy2~O~58XK(...etc)
b32 destination: c2ggbersn32rb62r5556bvzi757qsifdxuu5wumqgi5hfzezpb2q.b32.i2p
Read: http 1.1 .... etc etc etc (the http-servers's response)
```

It is also possible to create listening tunnels and accepting on them.

```go

	bob, _ := NewBOBController("tcp4", YourBOBController)
	keys, _ := bob.NewKeys()
	listener, _ := bob.Listen("MyIngoingTunnel", keys, 2, 2)
	for {
		conn, _ := listener.Accept()
		handleClient(conn)
	}
```

All error handling was ofc emitted from the above code for readability.

### Bugs ###
* If you try to create a new tunnel that already exist, it will fail. You will need to destroy the tunnel and then create it again. There should probably be a method to take ownership of tunnels that already exist.

Also, this is not production ready. It probably has bugs, somewhere.

### Testing ###
* `go test` will run all the tests. 
* `go test -short` will not dial real I2P addresses, so takes much less time. It will still dial temporary I2P addresses created for testing purposes though..

### Setup ###

`import "bitbucket.org/kallevedin/ccondom`

It depends only on the standard go libraries. 

And ofc that you have an I2P router up and running that you can connect to, somewhere.

### License ###
Public domain.

### Author ###

Kalle Vedin `kalle.vedin@fripost.org`
