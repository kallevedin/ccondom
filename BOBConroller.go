package ccondom

import (
	"bufio"
	"errors"
	"net"
	"strconv"
	"math/rand"
	"sync"
	"strings"
//	"fmt"
	"time"
)



// A BOB proxy, which is used for making connections through I2P.
type BOBController struct {
	Network     string              // how to connect to it
	Address     string              // address of BOB control port
	tunnels     map[string]int      // map of tunnel that are active
	randomStr   string   			// used to generate unique names for the I2P-tunnels
	bobCTRL     net.Conn            // connects to the I2P BOB control socket
	err         error

	control  chan(bobCommandStatus) // for controlling the bobController
	request  chan(bobCommands)      // sends lists of commands to execute at the BOB proxy
	
	commandChains []bobCommands     // command chains that are waiting to be processed
	currentChain  bobCommands       // current command chain
	empty         bool              // true if the chains above is empty
	
	l           sync.RWMutex        // lock guarding this struct
}



// used between the communicating goroutines
type bobCommandStatus struct {
	status      string
	message     string
}

type bobCommands struct {
	commands          []string
	replyTo           chan(bobComReply)
	handleFailure     bool                       // true if the bobController should issue "clear" commands upon failure on this chain
}

type bobComReply struct {
	err         error
    message     string
}



// Creates a new I2P BOB proxy
func NewBOBController(network, address string) (*BOBController, error) {
	duration, _ := time.ParseDuration("10s")
	conn, err := net.DialTimeout(network, address, duration)
	if err != nil {
		return nil, errors.New("Could not connect to BOB: " + err.Error())
	}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	b := &BOBController{Network: network, Address: address, 
		tunnels: make(map[string]int), randomStr: strconv.Itoa(10000 + r.Intn(89999)), 
		request: make(chan bobCommands), bobCTRL: conn }
	b.control = make(chan bobCommandStatus)
	go b.bobParser()
	go b.bobController()
	return b, nil
}



// Destroys the BOBController - closes all tunnels associated with it and, if
// successful kills the associated goroutines. If zombie is true and it failed
// to close the tunnels, the BOBController is not destroyed, and you can use it
// as normal.
func (b *BOBController) Destroy(zombie bool) error {
	b.l.RLock()
	tunmap := make(map[string]int)
	for tun,id := range b.tunnels {
		tunmap[tun]=id
	}
	b.l.RUnlock()
	
	failtuns := make(map[string]int)
	for tun,id := range tunmap {
		if _,ok := b.closeTunnel(tun, 5); !ok {
			failtuns[tun]=id
		}
	}
	if len(failtuns) > 0 {
		badtuns := make(map[string]int)
		for tun,id := range failtuns {
			if _,ok := b.closeTunnel(tun, 15); !ok {
				badtuns[tun]=id
			}
		}
		if len(badtuns) > 0 {
			if !zombie {
				b.err = errors.New("BOBController is destroyed (but failed to close all tunnels).")
				b.control <- bobCommandStatus{"DIE", ""}
			} else {
				return errors.New("Failed to close all tunnels")
			}
		}
	}
	b.control <- bobCommandStatus{"DIE", ""}
	b.err = errors.New("BOBController destroyed.")
	return nil
}

// Closes a tunnel that is active at BOB. It is possible to close other tunnels
// than the ones you have created with this BOBController.
func (b *BOBController) closeTunnel(tunnelName string, maxTries int) (error, bool) {
	b.l.RLock()
	if b.err != nil {
		b.l.RUnlock()
		return b.err, false
	}
	b.l.RUnlock()
	
	comms := []string{"getnick " + tunnelName, "stop", "clear"}
	rc := make(chan bobComReply)
	var reply bobComReply

	var tries int
	for tries:=0; tries!=maxTries; tries++ {
		b.request <- bobCommands{comms, rc, false}
		reply = <-rc
		if reply.err != nil {
			if strings.HasPrefix(reply.err.Error(), "getnick") {
				return errors.New("No tunnel exist with that name (" + tunnelName + ")"), true
			} else if reply.err.Error() == "clear" {
				if strings.Contains(reply.message, " active") {
					comms = []string{"getnick " + tunnelName, "stop"}
				} else {
					comms = []string{"getnick " + tunnelName, "stop", "clear"}
				}
			} else if reply.err.Error() == "stop" {
				if strings.Contains(reply.message, "inactive") {
					comms = []string{"getnick " + tunnelName, "clear"}
				} else {
					comms = []string{"getnick " + tunnelName, "stop", "clear"}
				}
			} else {
				// Sometimes it takes a while to close the tunnel, and clearing it
				// while waiting causes an error. Try again, after a little while.
				comms = []string{"getnick " + tunnelName, "clear"}
			}
			time.Sleep(750 * time.Millisecond)
		} else {
			break
		}
	}
	if tries == maxTries {
		return errors.New("Failed to close tunnel " + tunnelName), false
	}
	b.l.Lock()
	delete(b.tunnels, tunnelName)
	b.l.Unlock()
	return nil, true
}


func (b *BOBController) bobController() {
	insertedClear := false
	thisCommand := ""
	
	// If set to true, bobController will immediatly send a "clear" message to 
	// BOB if there was an error, which often means undoing a whole command
	// chain.
	handleFailure := true
	
	for {
		select {
			case cmd := <-b.control :
				if cmd.status == "OK" {
					if insertedClear {
						// clear executed successfully, so discard current chain
		//				fmt.Println("\tclear was successful, going to next chain")
						if b.nextChain() {
							if thisCommand,ok := b.nextCommand(); ok {
								b.bobCTRL.Write([]byte(thisCommand + "\n"))
							}
						}
						insertedClear = false
					} else {
						var ok bool
						if thisCommand,ok = b.nextCommand(); !ok {
		//					fmt.Println("\t--whole chain was OK, sending SUCCESS!")
							b.currentChain.replyTo <- bobComReply{err: nil, message: cmd.message}
							if b.nextChain() {
		//						fmt.Println("\tgoing to next chain")
								thisCommand,_ = b.nextCommand()
								b.bobCTRL.Write([]byte(thisCommand + "\n"))
							} else {
		//						fmt.Println("\tNo more commands.")
								// empty!
								thisCommand = ""
								handleFailure = true
							}
						} else {
							b.bobCTRL.Write([]byte(thisCommand + "\n"))
						}
					}
				} else if cmd.status == "DIE" {
					b.bobCTRL.Close()
					return
				} else if cmd.status == "ERROR" {
					if insertedClear {
						insertedClear = false
					}
		//			fmt.Println("\tGot error: " + cmd.message)
					b.currentChain.replyTo <- bobComReply{errors.New(thisCommand), cmd.message}
					if handleFailure {
		//			fmt.Println("\tSending clear!")
						b.bobCTRL.Write([]byte("clear\n"))
						insertedClear = true
					}
				} else {
					panic("bobController() Was sent unknown command through channel bobCommandStatus")
				}
			case coms := <-b.request :
//				fmt.Println("incomming bobCommands")
				handleFailure = coms.handleFailure
				b.l.Lock()
				if len(b.currentChain.commands) == 0 && len(b.commandChains) == 0 {
					b.currentChain = coms
					b.l.Unlock()
					if thisCommand, ok := b.nextCommand(); ok {
						b.bobCTRL.Write([]byte(thisCommand + "\n"))
					}
				} else {
					b.commandChains = append(b.commandChains, coms)
					b.l.Unlock()
				}
		}
	}
}



func (b *BOBController) bobParser() {
	gotFirstOK := false
	scanner := bufio.NewScanner(b.bobCTRL)
	for {
		if !scanner.Scan() {
			err := scanner.Err()
			if err == nil { // connection closed
				b.l.Lock()
				b.err = errors.New("Connection to BOB control port closed.")
				b.l.Unlock()
				return
			} else {
				b.l.Lock()
				b.err = errors.New("Could not read from BOB control port: " + err.Error())
				b.l.Unlock()
				return
			}
			b.control <- bobCommandStatus{"DIE", "Could not connect to BOB control port."}
		}
		line := scanner.Text()
		wordScanner := bufio.NewScanner(strings.NewReader(line))
		wordScanner.Split(bufio.ScanWords)

		if !wordScanner.Scan() {
			// empty line
			continue
		}
		command := wordScanner.Text()
		switch command {

		case "BOB":
			// gives version number, but we ignore that
			continue

		case "OK":
			// BOB sends an initial OK message, to say we are successfully connected.
			// dont generate message becasue of that
			if gotFirstOK {
				b.control <- bobCommandStatus{"OK", line}
			} else {
				gotFirstOK = true
			}

		case "ERROR":
			b.control <- bobCommandStatus{"ERROR", line}

		default:
			b.control <- bobCommandStatus{"OTHER", line}
		}
	}
}




// Moves to the next command chain, discards current. Returns true if successful,
// signaling that there are more commands. (false = no more commands!)
func (b *BOBController) nextChain() bool {
	b.l.Lock()
	defer b.l.Unlock()

	if len(b.commandChains) > 0 {
		b.currentChain = b.commandChains[0]
		b.commandChains = b.commandChains[1:]
		return true
	} else {
		return false
	}
}

// returns the next command and true if successful, and "" false if there were
// no more commands in current chain.
func (b *BOBController) nextCommand() (string, bool) {
	b.l.Lock()
	defer b.l.Unlock()
	
	if len(b.currentChain.commands) > 0 {
		r := b.currentChain.commands[0]
		b.currentChain.commands = b.currentChain.commands[1:]
		return r, true
	} else {
		return "", false
	}
}

func (b *BOBController) getRemoteAddr() string {
	b.l.RLock()
	rhost,_,_ := net.SplitHostPort(b.bobCTRL.RemoteAddr().String())
	b.l.RUnlock()
	return rhost
}

func (b *BOBController) getLocalAddr() string {
	b.l.RLock()
	lhost,_,_ := net.SplitHostPort(b.bobCTRL.LocalAddr().String())
	b.l.RUnlock()
	return lhost
}



var (
	uniqueId         int          = 0                // globally unique id numbers for I2P-tunnels
	uniqueIdMutex    sync.RWMutex                    // locks uniqueId
)

// Returns a unique name.
func (b *BOBController) newTunnelName() string {
	uniqueIdMutex.Lock()
	id := uniqueId
	uniqueId += 1
	uniqueIdMutex.Unlock()
	return "CC" + b.randomStr + "-" + strconv.Itoa(id)
}
