// Copyright 2010 The Go Authors.  All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Modified net.Pipe to become ccondom.BOBPipe.

package ccondom

import (
	"errors"
	"time"
	"net"
)

// Pipe creates a synchronous, in-memory, full duplex
// network connection; both ends implement the Conn interface.
// Reads on one end are matched with writes on the other,
// copying data directly between the two; there is no internal
// buffering.
func newBOBPipe(local, remote string, id int) (net.Conn, net.Conn) {
	r1, w1 := newPipe(id, bobPipeClose)
	r2, w2 := newPipe(id, bobPipeClose)

	return &bobPipe{local, remote, id, r1, w2}, &bobPipe{local, remote, id, r2, w1}
}

type bobPipe struct {
	local      string
	remote     string
	id         int
	*pipeReader
	*pipeWriter
}

type bobPipeAddr string

func (bobPipeAddr) Network() string {
	return "I2P"
}

func (b bobPipeAddr) String() string {
	return string(b)
}

func (p *bobPipe) Close() error {
	err := p.pipeReader.close()
	err1 := p.pipeWriter.close()
	if err == nil {
		err = err1
	}
	return err
}

func (p *bobPipe) LocalAddr() net.Addr {
	return bobPipeAddr(p.local)
}

func (p *bobPipe) RemoteAddr() net.Addr {
	return bobPipeAddr(p.remote)
}

func (p *bobPipe) SetDeadline(t time.Time) error {
	return errors.New("net.Pipe does not support deadlines")
}

func (p *bobPipe) SetReadDeadline(t time.Time) error {
	return errors.New("net.Pipe does not support deadlines")
}

func (p *bobPipe) SetWriteDeadline(t time.Time) error {
	return errors.New("net.Pipe does not support deadlines")
}

