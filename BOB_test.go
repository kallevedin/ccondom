package ccondom

import (
//	"bufio"
	"encoding/hex"
	"fmt"
	"testing"
	"strings"
	"time"
	"runtime"
	)

var (
	YourBOBController	string = "127.0.0.1:2827"
	I2Paddr1		string = "irc.dg.i2p"
	I2Paddr2        string = "zzz.i2p" // "i2host.i2p" // "forum.i2p" 
	I2Paddr3		string = "7vzui6y5lm2d3hc2oie54tlcosn7zkl5skfxepv7auqh3rxjemmq.b32.i2p" // has an I2PAddr > 516 bytes due to certificate
	)

func TestLookup(t *testing.T) {
    bob, err := NewBOBController("tcp4", YourBOBController)
    if err != nil {
        fmt.Println("Maybe you are not running I2P with BOB enabled.")
        fmt.Println(err.Error())
        t.Fail()
        return
    }
	addr, err := bob.Lookup(I2Paddr2)
    if err != nil {
        fmt.Println("Lookup failed.")
        fmt.Println(err.Error())
        t.Fail()
        return
    }
	fmt.Println(I2Paddr2 + " = " + addr.Base32())
	bob.Destroy(false)
}

func TestBOBController(t *testing.T) {
	fmt.Println("Attaching to BOB at " + YourBOBController)
	bob, err := NewBOBController("tcp4", YourBOBController)
	if err != nil {
		fmt.Println("Maybe you are not running I2P with BOB enabled.")
		fmt.Println(err.Error())
		t.Fail()
		return
	}	
	
	fmt.Println("Creating tunnel.")
	tunnel, err := bob.NewOutgoingTunnel("", 0, 0) // for testing only
	if err != nil {
		fmt.Println("Failed to create tunnel: " + err.Error())
		t.Fail()
		return
	}
	fmt.Println("Tunnel created. It has the name: " + tunnel.Name)

	if !testing.Short() { ////////////////////////////////////////////////////// omitted when test -short
		buf := make([]byte, 256)

		fmt.Println("Dialing an I2P address: " + I2Paddr1)
		conn, err := tunnel.DialI2P(I2Paddr1)
		if err != nil {
			fmt.Println("Failed to dial to " + I2Paddr1)
			fmt.Println(err.Error())
			t.Fail()
		} else {
			conn.Write([]byte("NICK awe777\r\nUSER awe777 0 * :awe777\r\n"))
			n,err := conn.Read(buf)
			if err != nil {
				fmt.Println("Failed to read from .i2p addr")
				t.Fail()
			}
			if strings.Contains(strings.ToLower(string(buf[:n])), "ping") {
				println("Looks like we connected to the IRC server listening at " + I2Paddr1)
			} else {
				println("If this looks like IRC it was a success: " + string(buf[:n]))
			}
			conn.Close()
		}

		fmt.Println("Dialing an I2P address: " + I2Paddr2)
		conn, err = tunnel.DialI2P(I2Paddr2)
		if err != nil {
			fmt.Println("Failed to dial to " + I2Paddr2)
			fmt.Println(err.Error())
			t.Fail()
		} else {
			conn.Write([]byte("GET / HTTP/1.1\n\n"))
			n, err := conn.Read(buf)
			if err != nil {
				fmt.Println("Failed to read from .i2p addr")
				t.Fail()
			}
			if strings.Contains(strings.ToLower(string(buf[:n])), "http") {
				println("Looks like we received a HTTP response from " + I2Paddr2)
			} else {
				println("If this looks like HTTP it was a success: " + string(buf[:n]))
			}
			conn.Close()
		}

		fmt.Println("Performing lookup at " + I2Paddr3)
		addr, err := bob.Lookup(I2Paddr3)
		if err != nil {
			fmt.Println("Failed to perform lookup at " + I2Paddr3)
			t.Fail()
		} else {
			shortVersion := addr[:50] + "(...etc)"
			//fmt.Printf("len(addr)=%d\nlongversion:%s", len(addr), addr)
			fmt.Println("Translated to: " + shortVersion + ". Dailing it!")
			conn, err = tunnel.DialI2PAddr(addr)
			if err != nil {
				fmt.Println("Failed to dial to " + shortVersion)
				fmt.Println(err.Error())
				t.Fail()
			} else {
				conn.Write([]byte("GET / HTTP/1.1\n\n"))
				n,err := conn.Read(buf)
				if err != nil {
					fmt.Println("Failed to read from .i2p addr")
					t.Fail()
				}
				if strings.Contains(strings.ToLower(string(buf[:n])), "http") {
					println("Looks like we received a HTTP response from " + I2Paddr3)
				} else {
					println(string(buf[:n]))
				}
				conn.Close()
			}
		}
	} ////////////////////////////////////////////////////////////////////////// end of short omission

	fmt.Printf("Closing tunnel: " + tunnel.Name + "...")
	err = tunnel.Close()
	if err != nil {
		println(err.Error())
		t.Fail()
	}
	bob.Destroy(false)
	fmt.Println("Closed OK.")
}



func TestDestroyBOBWithManyTunnels(t *testing.T) {

	if testing.Short() {
		return
	}
	// simultanous shit
	ncpu := runtime.NumCPU()
	runtime.GOMAXPROCS(ncpu + 1)

	fmt.Println("Attaching to BOB at " + YourBOBController)
	bob, err := NewBOBController("tcp4", YourBOBController)
	if err != nil {
		fmt.Println("Maybe you are not running I2P with BOB enabled.")
		fmt.Println(err.Error())
		t.Fail()
		return
	}	
	
	fmt.Println("If this fails, restart BOB...")
	gr := 15                    // 15 minus one tunnels!
	ch := make(chan bool)
	for i:=0; i!=gr; i++ {
		if i == 5 {
			go func(id int, ok chan(bool)) {       // one lookup just to add something else
				bob.NewKeys()
				_, err := bob.Lookup("zzz.i2p")
				if err!=nil {
					fmt.Println("Lookup failed.")
					ok <- false
				} else {
					fmt.Println("also successfully looked up zzz.i2p in the middle of it all.")
					ok <- true
				}
			}(i, ch)
		} else {
			go func(id int, ok chan(bool)) {
				fmt.Println("Creating tunnel.")
				tunnel, err := bob.NewOutgoingTunnel("", 0, 0) // for testing only
				if err != nil {
					fmt.Println("Failed to create tunnel: " + err.Error())
					ok <- false
					return
				}
				fmt.Println("Tunnel created. It has the name: " + tunnel.Name)
				ok <- true
			}(i, ch)
		}
	}
	for i:=0; i!=gr; i++ {
		<-ch
	}
	fmt.Println("Destroying BOBController, which destroys all those ^ tunnels. This might take a while.")
	if err := bob.Destroy(true); err!=nil {
		fmt.Println("Failed to destroy BOBController. It is in zombie mode: " + err.Error())
		if err := bob.Destroy(false); err!=nil {
			fmt.Println("Forced destruction of BOBController. Tunnels are still active!")
			t.Fail()
			return
		} else {
			fmt.Println("2nd attempt at destroying BOBController worked!")
			return
		}
	}
	fmt.Println("BOBController destroyed.")
}

func Test_Keys(t *testing.T) {
	fmt.Println("Attaching to BOB at " + YourBOBController)
	bob, err := NewBOBController("tcp4", YourBOBController)
	if err != nil {
		fmt.Println("Maybe you are not running I2P with BOB enabled.")
		fmt.Println(err.Error())
		t.Fail()
		return
	}
	fmt.Println("Generating new keys.")
	keys, err := bob.NewKeys()
	if err != nil {
		fmt.Println("Failed to generate keys!")
		fmt.Println(err.Error())
		t.Fail()
		return
	}
	shortVersion := keys[:50] + "(...etc)"
	fmt.Printf("Keys: %s\n", shortVersion)
	fmt.Println("Getting destination of those keys.")
	I2Paddr, err := bob.GetI2PAddr(keys)
	if err != nil {
		fmt.Println("Failed to extract destination!")
		fmt.Println(err.Error())
		t.Fail()
		return
	}
	fmt.Printf("b32 version of destination: " + I2Paddr.Base32() + "\n")
	fmt.Println("Destroying BOB")
	if err := bob.Destroy(false); err != nil {
		fmt.Println("Failed to destroy BOB.")
		t.Fail()
	} else {
		fmt.Println("BOB destroyed.")
	}
}


func TestListen(t *testing.T) {
	fmt.Println("Attaching to BOB at " + YourBOBController)
	bob, err := NewBOBController("tcp4", YourBOBController)
	if err != nil {
		fmt.Println("Maybe you are not running I2P with BOB enabled.")
		fmt.Println(err.Error())
		t.Fail()
		return
	}
	fmt.Println("Generating new keys.")
	keys, err := bob.NewKeys()
	if err != nil {
		fmt.Println("Failed to generate keys!")
		fmt.Println(err.Error())
		t.Fail()
		return
	}
	shortVersion := keys[:50] + "(...etc)"
	fmt.Printf("Keys: %s\n", shortVersion)
	fmt.Println("Getting destination of those keys.")
	I2Paddr, err := bob.GetI2PAddr(keys)
	if err != nil {
		fmt.Println("Failed to extract destination!")
		fmt.Println(err.Error())
		t.Fail()
		return
	}
	fmt.Printf("b32 version of destination we created: " + I2Paddr.Base32() + "\n")
	l, err := bob.Listen("", keys, 0, 0)
	if err != nil {
		fmt.Println("Failed to create listener!")
		fmt.Println(err.Error())
		t.Fail()
		return
	}
	caddr := make(chan string)
	go func(addr I2PAddr, laddr chan(string)) {
		time.Sleep(5 * time.Second)
		fmt.Println("Dailing the address we are listening on.")
		tunnel, err := bob.NewOutgoingTunnel("", 0, 0) // for testing only
		if err != nil {
			fmt.Println("Failed to create tunnel: " + err.Error() + ". You need to abort this test by pressing CTRL+C.")
			t.Fail()
			return
		}
		conn, err := tunnel.DialI2PAddr(addr)
		if err != nil {
			fmt.Println("Failed to dial to " + addr + ". You need to abort this test by pressing CTRL+C.")
			fmt.Println(err.Error())
			t.Fail()
			return
		} else {
			addr, err := NewI2PAddrFromString(conn.LocalAddr().String())
			if err != nil {
				fmt.Println("Failed to read connecting tunnels I2P address!")
				t.Fail()
			} else {
				fmt.Println("Connecting tunnels local address: " + addr.Base32())
			}
			conn.Write([]byte("Hello world! <3 <3 <3 <3 <3 <3 <3 <3"))
			conn.Close()
			tunnel.Close()
			laddr <- addr.Base32()
			return
		}
	}(I2Paddr, caddr)
	
	conn, err := l.Accept()
	if err != nil {
		fmt.Println("Failed to accept!")
		fmt.Println(err.Error())
		t.Fail()
		return		
	}
	
	buf := make([]byte, 1024)
	n, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Failed to read from I2PListener! " + err.Error())
		t.Fail()
		return
	}
	raddr, err := NewI2PAddrFromString(conn.RemoteAddr().String())
	if err != nil {
		fmt.Println("Could not determine conneding tunnels remote address")
		t.Fail()
	} else {
		fmt.Printf("Received connection from %s\n", raddr.Base32())
	}
	fmt.Printf("RECEIVED (from I2P): %s\n", string(buf[:n]))

	if raddr.Base32() != (<-caddr) {
		fmt.Println("Failure to decode connecting peers I2P address.")
		t.Fail()
	} else {
		fmt.Println("Successfully decoded connecting peers I2P address.")
	}

	if err := l.Close(); err != nil {
		t.Fail()
		fmt.Println("Could not close listener: " + err.Error())
	}

	fmt.Println("Destroying BOB")
	if err := bob.Destroy(false); err != nil {
		fmt.Println("Failed to destroy BOB.")
		t.Fail()
	} else {
		fmt.Println("BOB destroyed.")
	}
}


func TestI2PConn(t *testing.T) {
	fmt.Println("Attaching to BOB at " + YourBOBController)
	bob, err := NewBOBController("tcp4", YourBOBController)
	if err != nil {
		fmt.Println("Maybe you are not running I2P with BOB enabled.")
		fmt.Println(err.Error())
		t.Fail()
		return
	}
	fmt.Println("Generating new keys.")
	keys, err := bob.NewKeys()
	if err != nil {
		fmt.Println("Failed to generate keys!")
		fmt.Println(err.Error())
		t.Fail()
		return
	}
	shortVersion := keys[:50] + "(...etc)"
	fmt.Printf("Keys: %s\n", shortVersion)
	fmt.Println("Getting destination of those keys.")
	I2Paddr, err := bob.GetI2PAddr(keys)
	if err != nil {
		fmt.Println("Failed to extract destination!")
		fmt.Println(err.Error())
		t.Fail()
		return
	}
	fmt.Printf("b32 version of destination we created: " + I2Paddr.Base32() + "\n")
	if I2Paddr.String() != keys.Addr().String() {
		fmt.Printf("I2PKeys.Addr() failed.\n")
		fmt.Println("(hex) I2Paddr: \t\t\"" + hex.EncodeToString([]byte(I2Paddr.String())) + "\"")
		fmt.Println("(hex) keys.Addr().String()\t\"" + hex.EncodeToString([]byte(keys.Addr().String())) + "\"")
		t.Fail()
	} else {
		fmt.Println("keys.Addr() replies the same as bob.GetI2PAddr(keys) -- thats good!")
	}
	
	fmt.Println("Destroying BOB")
	if err := bob.Destroy(false); err != nil {
		fmt.Println("Failed to destroy BOB.")
		t.Fail()
	} else {
		fmt.Println("BOB destroyed.")
	}
}
